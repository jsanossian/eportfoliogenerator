    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Page;
import java.util.Vector;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author jsanossian
 */
public class PageView extends BorderPane{
    //Page page;
    EditPageStage editPageStage;
    VBox titleVBox;
    Button removeButton;
    Label titleLabel;
    TextField titleTextField;
    Button editButton, selectButton;
    Page page;
    EportfolioGeneratorView ui;
    public PageView(Page page, EportfolioGeneratorView ui){
        this.page = page;
        this.ui = ui;
        titleVBox = new VBox();
        titleLabel = new Label("Title: ");
        titleTextField = new TextField();
        //titleTextField.setText(page.getTitle());
        titleVBox.getChildren().add(titleLabel);
        titleVBox.getChildren().add(titleTextField);
        this.setTop(titleVBox);
        removeButton = new Button("REMOVE");
        editButton = new Button("EDIT");
        this.setLeft(removeButton);
        this.setRight(editButton);
        this.selectButton = new Button("SELECT");       
        this.setBottom(selectButton);
        titleTextField.setOnAction(e->{
            page.setTitle(titleTextField.getText());
            
            
        });
        removeButton.setOnAction(e->{
            ui.eportfolio.removePage(page);
            ui.reloadUI();
        });
        selectButton.setOnAction(e->{
            ui.eportfolio.setSelectedPage(page);
            ui.reloadUI();
        });
        editButton.setOnAction(e->{
            editPageStage = new EditPageStage(page);
        });
    }
    
}
