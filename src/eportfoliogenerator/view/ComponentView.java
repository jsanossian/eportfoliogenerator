/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Component;
import java.time.Clock;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author jsanossian
 */
public class ComponentView extends BorderPane{
    VBox titleVBox;
    Button removeButton;
    Label titleLabel;
    TextField titleTextField;
    Button editButton;
    Component c;
    EportfolioGeneratorView ui;
    public ComponentView(Component c, EportfolioGeneratorView ui) {
        this.c = c;
        this.ui = ui;
        
        titleVBox = new VBox();
        titleLabel = new Label(c.getType());
        
        //titleTextField.setText(page.getTitle());
        titleVBox.getChildren().add(titleLabel);
        titleVBox.setAlignment(Pos.CENTER);
        this.setTop(titleVBox);
        removeButton = new Button("REMOVE");
        editButton = new Button("EDIT");
        this.setLeft(removeButton);
        this.setRight(editButton);
        removeButton.setOnAction(e->{
            ui.eportfolio.getSelectedPage().removeComponent(c);
            ui.reloadUI();
        });
        editButton.setOnAction(e->{
            if(c.getType().equalsIgnoreCase("paragraph"))
                new EditParagraphComponentStage(c);
            else if(c.getType().equalsIgnoreCase("header"))
                new EditHeaderComponentStage(c);
            else if(c.getType().equalsIgnoreCase("list"))
                new EditListComponentStage(c);
            else if(c.getType().equalsIgnoreCase("image"))
                new EditImageComponentStage(c);
            else if(c.getType().equalsIgnoreCase("video"))
                new EditVideoComponentStage(c);
            else if(c.getType().equalsIgnoreCase("slideshow"))
                new EditSlideshowComponentStage();
            else System.out.print("incorrect component");
        });
    }
    
}


