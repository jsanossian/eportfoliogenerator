/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.EportfolioModel;
import eportfoliogenerator.model.Page;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class AddComponentStage extends Stage{
    Scene primaryScene;
    ChoiceBox choiceBox;
    BorderPane pane;
    Button enterButton, cancelButton;
    Label label;
    VBox choicePane;
    Page page;
    public AddComponentStage(Page page) {
        this.page = page;
        pane = new BorderPane();
        choicePane = new VBox();
        label = new Label("Select which type of Component  to add: ");
        enterButton = new Button("ENTER");
        cancelButton = new Button("CANCEL");
        choiceBox = new ChoiceBox();
        choiceBox.getItems().addAll("Paragraph", "Header", "List", "Image", "Video", "SlideShow");
        choiceBox.setValue("Paragraph");
        choicePane.getChildren().addAll(label, choiceBox, new VBox());
        choicePane.setSpacing(9);
        pane.setTop(choicePane);
        pane.setLeft(cancelButton);
        pane.setRight(enterButton);
        addEventHandlers();
        
        
        primaryScene = new Scene(pane);
        this.setScene(primaryScene);
        this.showAndWait();
    }
    
    private void addEventHandlers() {
      cancelButton.setOnAction(e->{
          this.close();
      });
        enterButton.setOnAction(e-> {
          String choice = ((String) choiceBox.getValue()).trim();
          System.out.println(choice);
          if(choice.equalsIgnoreCase("Paragraph")){
              new AddParagraphComponentStage(page);
          }
          if(choice.equalsIgnoreCase("Header")){
               new AddHeaderComponentStage(page);
          }
          else if(choice.equalsIgnoreCase("List")){
               new AddListComponentStage(page);
          }
          else if(choice.equalsIgnoreCase("Image")){
               new AddImageComponentStage(page);
          }
          else if(choice.equalsIgnoreCase("Video")){
               new AddVideoComponentStage(page);
          }
          else if(choice.equalsIgnoreCase("Slideshow")){
               new AddSlideshowComponentStage(page);
          }
          else{}
          this.close();
      });
        
    }
 
}
