/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.Page;
import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class AddVideoComponentStage extends Stage{
    Scene primaryScene;
    VBox pane;
    HBox fileHbox, srcHbox;
    Label fileLabel, srcLabel, src;
    Button fileButton, enterButton;
   HBox widthHbox;
    HBox heightHbox;
     Label widthLabel;
   Label heightLabel;
     TextField widthField;
     TextField heightField;
     Page page;
    public AddVideoComponentStage(Page page) {
        this.page = page;
        pane = new VBox(8);
        fileHbox = new HBox(20);
        fileLabel = new Label("Select Video file: ");
        fileButton = new Button("SELECT FILE");
        fileHbox.getChildren().addAll(fileLabel, fileButton);
        
        widthHbox = new HBox(5);
        heightHbox = new HBox(5);
        widthLabel = new Label("Enter the height of the Video(10-1000): ");
        heightLabel = new Label("Enter the height of the Video(10-1000): ");
        widthField = new TextField("100");
        heightField = new TextField("100");   
        widthHbox.getChildren().addAll(widthLabel, widthField);
        heightHbox.getChildren().addAll(heightLabel, heightField);
        
        srcHbox = new HBox(5);
        srcLabel = new Label("File src: ");
        src = new Label("absolutefilepath.mp4");
        srcHbox.getChildren().addAll(srcLabel, src);
         fileButton.setOnAction(e-> {
            FileChooser imageFileChooser = new FileChooser();
        File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	   src.setText(file.toString());
	}
        
        });
        enterButton = new Button("ENTER");
        enterButton.setOnAction(e->{
        Component c = new Component();
        int width = 0;
        int height = 0;
        width = Integer.parseInt(widthField.getText().trim());
        height = Integer.parseInt(heightField.getText().trim());
        if (width<10) width = 10;
        if(width>1000) width = 1000;
        if(height<10) height = 10;
        if(height>1000) height =1000;
        c.setType("video");
        c.setText(src.getText());
        c.setWidth(width);
        c.setHeight(height);
        c.setFont("");
        this.page.addComponent(c);
        this.close();
        });
        pane.getChildren().addAll(fileHbox,widthHbox, heightHbox, srcHbox,enterButton);
        
        primaryScene = new Scene(pane);
        this.setScene(primaryScene);
        this.showAndWait();
    }
    
}
