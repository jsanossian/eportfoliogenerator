/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.EportfolioModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class LoadEportfolioStage extends Stage{
    EportfolioGeneratorView ui;
    public LoadEportfolioStage(EportfolioGeneratorView ui) {
        this.ui = ui;
        FileChooser slideShowFileChooser = new FileChooser();
       // slideShowFileChooser.setInitialDirectory(new File("eportfolios/"));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());
        if (selectedFile != null) {
            try {
            FileInputStream fileIn = new FileInputStream(selectedFile);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            Object obj = objectIn.readObject();
           if(obj instanceof EportfolioModel){
               EportfolioModel e = (EportfolioModel) obj;
               Label l = new Label("Eportfolio was loaded successfully");
               this.setScene(new Scene(new VBox(l)));
               ui.eportfolio = e;
               ui.reloadUI();
               this.showAndWait();
           }
           else{
               Label l = new Label("INVALID FILE!: Eportfolio was not loaded");
               this.setScene(new Scene(new VBox(l)));
               this.showAndWait();
           }
            } catch (Exception e) {
               
		
            }
        }
    }
    
}
