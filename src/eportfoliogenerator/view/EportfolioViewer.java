/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.EportfolioModel;
import java.io.File;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class EportfolioViewer extends Stage{
    EportfolioModel eportfolio;
    
    public EportfolioViewer(EportfolioModel eportfolio){
        String title = eportfolio.getPage(0).getStudentName();
        this.setTitle(title +"'s Eportfolio");
        //create webview
        WebView webview = new WebView();
        File html =  new File("./sites/" + title + "/index.html");
        Scene scene = new Scene(webview);
       webview.getEngine().setJavaScriptEnabled(true);
       System.out.print(html.getAbsolutePath());
         webview.getEngine().load("file:" + html.getAbsolutePath());
         File css = new File("./sites/" + title+ "/css/layout1.css");
         if(css.exists()){
             webview.getStylesheets().add(css.toPath().toAbsolutePath().toString());
         }
         css = new File("./sites/" + title+ "/css/color1.css");
         if(css.exists()){
             webview.getStylesheets().add(css.toPath().toAbsolutePath().toString());
         }
        
        this.setMinHeight(720);
        this.setMinWidth(720);
        this.setScene(scene);
        this.show();
        
    }
            
}
