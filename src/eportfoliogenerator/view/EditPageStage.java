/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.EportfolioModel;
import eportfoliogenerator.model.Page;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class EditPageStage extends Stage{
    Scene primaryScene;
    GridPane pane;
    Label nameLabel, bannerLabel, footerLabel, layoutLabel, colorLabel;
    CheckBox bannerCheck;
    TextField name, footer;
    ChoiceBox layoutChoiceBox, colorChoiceBox;
    Button selectFileButton, cancelButton, enterButton;
    Page page;
    public EditPageStage(Page page) {
        this.page = page;
        pane = new GridPane();
        nameLabel = new Label("Enter the Student Name: ");
        bannerLabel = new Label("Check the box to have a banner and select a file: ");
        footerLabel = new Label("Enter the text for the footer: ");
        layoutLabel = new Label("Select a layout: ");
        colorLabel = new Label("Select a color scheme: ");
        name = new TextField();
        footer = new TextField();
        layoutChoiceBox = new ChoiceBox();
        layoutChoiceBox.getItems().addAll(1,2,3,4,5);
        colorChoiceBox = new ChoiceBox();
        colorChoiceBox.getItems().addAll(1,2,3,4,5);
        selectFileButton = new Button("SELECT FILE");
        bannerCheck = new CheckBox();
        enterButton = new Button("ENTER");
        cancelButton = new Button("CANCEL");
        enterButton.setOnAction(e->{
           
           page.setStudentName(name.getText());
           page.setFooter(footer.getText());
           page.setLayout(Integer.parseInt(layoutChoiceBox.getValue().toString()));
           page.setColor(Integer.parseInt(colorChoiceBox.getValue().toString()));
            this.close();
        });
        cancelButton.setOnAction(e->{
            this.close();
        });
        pane.add(nameLabel, 0, 0);
        pane.add(name, 1, 0);
        pane.add(bannerLabel, 0, 1);
        pane.add(selectFileButton, 1, 1);
        pane.add(bannerCheck, 2, 1);
        pane.add(footerLabel, 0, 2);
        pane.add(footer, 1, 2);
        pane.add(layoutLabel, 0, 3);
        pane.add(layoutChoiceBox, 1, 3);
        pane.add(colorLabel, 0, 4);
        pane.add(colorChoiceBox, 1, 4);
        pane.add(cancelButton, 0, 6);
        pane.add(enterButton, 1, 6);
        
        primaryScene = new Scene(pane);
        this.setScene(primaryScene);
        this.showAndWait();
        
    }
    
}
