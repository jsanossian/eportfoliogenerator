/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.Page;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class AddHeaderComponentStage extends Stage{
    Scene primaryScene;
    VBox pane;
    HBox fontPane;
    Label paragraphLabel, fontLabel;
    ChoiceBox fontChoiceBox;
    Button enterButton;
    TextArea paragraph;
    Page page;
    public AddHeaderComponentStage(Page page) {
        this.page = page;
        pane = new VBox(8);
        fontPane = new HBox(5);
        paragraphLabel = new Label("Enter the text for the header: ");
        paragraph = new TextArea();
        paragraph.setMinHeight(90);
        paragraph.setMaxWidth(200);
        paragraph.setWrapText(true);
        fontChoiceBox = new ChoiceBox();
        fontChoiceBox.getItems().addAll("Times New Roman", "Engagement", "Calligraffiti", "Serif", "Butterfly Kids");
        fontChoiceBox.setValue("Times New Roman");
        fontLabel = new Label("Enter a font: ");
        enterButton = new Button("ENTER");
        enterButton.setOnAction(e->{
        Component c = new Component();
        c.setType("header");
        c.setText(paragraph.getText());
        c.setWidth(0);
        c.setHeight(0);
        c.setFont(fontChoiceBox.getSelectionModel().getSelectedItem().toString());
        this.page.addComponent(c);
        this.close();
        });
        
        pane.getChildren().add(paragraphLabel);
        pane.getChildren().add(paragraph);
        fontPane.getChildren().addAll(fontLabel, fontChoiceBox);
        pane.getChildren().addAll(fontPane, enterButton);
        
        
        primaryScene = new Scene(pane);
        this.setScene(primaryScene);
        this.showAndWait();
    }
    
}
