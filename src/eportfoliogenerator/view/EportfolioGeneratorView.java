/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eportfoliogenerator.view;
import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.EportfolioModel;
import eportfoliogenerator.model.Page;
import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
/**
 *
 * @author jsanossian
 */
public class EportfolioGeneratorView {
    
    Stage primaryStage;
    Scene primaryScene;
    
    BorderPane egPane;
    
    
   
    //File toolbar layout
    HBox fileToolbarPane;
    Button newButton, loadButton, saveButton, saveAsButton, exportButton,viewButton,exitButton;
   
    
    HBox workspace;
    
    // Site Pages Layout
    
        // holds page editor and buttons
    VBox pagePane;
    ScrollPane pageScrollPane;
    
    VBox pageEditPane;
    HBox pageEditButtonPane;
    Button addPageButton;
    Button addComponentButton;
    // page componentLayout
    VBox componentEditPane;
    ScrollPane componentEditScrollPane;
    
    ComponentView sampleParagraph, sampleHeader, sampleList, sampleImage, sampleVideo, sampleSlideshow;
    EportfolioViewer viewer;
    //model
    EportfolioModel eportfolio;
    NewEportfolioStage newEportfolioStage;
    private AddPageStage addPageStage;
    private AddComponentStage addComponentStage;
    private LoadEportfolioStage loadEportfolioStage;
    private ExportEportfolioStage exportEportfolioStage;
    private VBox componentPane;
    
    //file manager
    
    //EportfolioFileManager fileManager;
    
    public EportfolioGeneratorView(){
         primaryStage = new Stage();
        eportfolio = new EportfolioModel();
    }
    
    public EportfolioModel getEportfolioModel(){
        return eportfolio;
    }
    
    public Stage getWindow(){
        return primaryStage;
    }
    
    public void startUI(Stage primaryStage, String windowTitle){
        initFileToolbar();
        initWorkSpace();
        initEventHandlers();
        this.primaryStage = primaryStage;
        initWindow(windowTitle);
    }

    private void initFileToolbar() {
        fileToolbarPane = new HBox(10);
        newButton = initChildButton(fileToolbarPane, false, "NEW");
        loadButton = initChildButton(fileToolbarPane, false, "LOAD");
        saveButton = initChildButton(fileToolbarPane, false, "SAVE");
        saveAsButton = initChildButton(fileToolbarPane, false, "SAVE AS");
        exportButton = initChildButton(fileToolbarPane, false, "EXPORT");
        viewButton = initChildButton(fileToolbarPane, false, "VIEW");
        exitButton = initChildButton(fileToolbarPane, false, "EXIT");
    }
    private Button initChildButton(Pane toolbar, boolean disabled, String text) {
        Button button = new Button();
        button.setText(text);
        button.setDisable(disabled);
        toolbar.getChildren().add(button);
	return button;
    }
    private void initWorkSpace() {
        workspace = new HBox();
        
        pagePane = new VBox();
        pagePane.setMinWidth(200);
        
        pageEditPane = new VBox();
        addPageButton = initChildButton(pagePane, false, "ADD PAGE");
        pagePane.setCenterShape(true);
        pageScrollPane = new ScrollPane(pageEditPane);
        pagePane.getChildren().add(pageScrollPane);
        componentEditPane = new VBox(10);
        componentPane = new VBox();
        componentPane.setMaxWidth(200);
        componentEditScrollPane = new ScrollPane(componentEditPane);
       
        addComponentButton = initChildButton(componentPane, false, "ADD COMPONENT");
        componentPane.getChildren().add(componentEditScrollPane);
        
        
        workspace.getChildren().add(pagePane);
        workspace.getChildren().add(componentPane);
        
    }

    private void initEventHandlers() {
        //Delete before sending out final version
        
        
        
        //file toolbar stuff 
        //FileToolbar
        newButton.setOnAction(e -> {
            boolean save = promptToSave();
            newEportfolioStage = new NewEportfolioStage();
            eportfolio = new EportfolioModel();
            this.reloadUI();
        });
        loadButton.setOnAction(e ->{
           boolean save = promptToSave();
            if(save){
                loadEportfolioStage = new LoadEportfolioStage(this);
            }
        });
        exitButton.setOnAction(e -> {
            boolean save = promptToSave();
            System.exit(0);
               
        });
        exportButton.setOnAction(e -> {
            exportEportfolioStage = new ExportEportfolioStage(this.eportfolio);
            
        });
        saveButton.setOnAction(e -> {
            eportfolio.save("eportfolios/" + eportfolio.getPage(0).getStudentName());
        });
        saveAsButton.setOnAction(e -> {
            FileChooser slideShowFileChooser = new FileChooser();
            FileChooser.ExtensionFilter objFilter = new FileChooser.ExtensionFilter("Object files (*.obj)", "*.obj");
            slideShowFileChooser.getExtensionFilters().add(objFilter);
        //slideShowFileChooser.setInitialDirectory(new File("eportfolios/"));
        File selectedFile = slideShowFileChooser.showSaveDialog(this.getWindow());
        if(selectedFile!=null){
            eportfolio.save(selectedFile.getAbsolutePath());
            
        }
        });
        
        //Site(Page) Toolbar
        addPageButton.setOnAction(e -> {
            addPageStage = new AddPageStage(eportfolio);
            this.reloadUI();
        });
        
        //Component Toolbar
        addComponentButton.setOnAction(e -> {
            addComponentStage = new AddComponentStage(eportfolio.getSelectedPage());
            this.reloadUI();
        });
        
        viewButton.setOnAction(e->{
             exportEportfolioStage = new ExportEportfolioStage(this.eportfolio);
            viewer = new EportfolioViewer(eportfolio);
        });
    }

    private void initWindow(String windowTitle) {
        primaryStage.setTitle(windowTitle);
        
      // GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        
        egPane = new BorderPane();
        
        workspace.setPadding(new Insets(20, 20, 0, 0));
        egPane.setTop(fileToolbarPane);
        egPane.setCenter(workspace);
        
        
        primaryScene = new Scene(egPane);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    private boolean promptToSave() {
  
        return true;
    }
    public void reloadUI() {
	pageEditPane.getChildren().clear();
        componentEditPane.getChildren().clear();
	if(eportfolio.getPages()!= null){
        for (Page page: eportfolio.getPages()) {
	    PageView pageEditor = new PageView(page, this);
            pageEditor.titleTextField.setText(page.getTitle());
	    if (page.equals(eportfolio.getSelectedPage()))
		pageEditor.selectButton.setDisable(true);
	    else pageEditor.selectButton.setDisable(false);
            pageEditPane.getChildren().add(pageEditor);
        }
        }
        if(eportfolio.getSelectedPage() != null){
        for(Component c: eportfolio.getSelectedPage().getComponents()){
                ComponentView cEdit = new ComponentView(c, this);
                componentEditPane.getChildren().add(cEdit);
            }
        }
	}

}




/**
 * *****TODO
 * 1) tie add/edit stages and model together
 * 2) update(reload) gui to workspace and tie them to model
 * 3) page view and componentview events
 * 3) view button
 * 4) file toolbar
 */