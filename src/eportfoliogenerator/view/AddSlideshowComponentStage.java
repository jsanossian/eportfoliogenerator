/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.Page;
import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author jsanossian
 */
class AddSlideshowComponentStage extends Stage {

     Scene primaryScene;
    VBox pane;
    HBox fileHbox, srcHbox;
    Label fileLabel, srcLabel, src;
    Button fileButton, enterButton;
    Page page;
    public AddSlideshowComponentStage(Page page) {
        pane = new VBox(8);
        this.page = page;
        fileHbox = new HBox(20);
        fileLabel = new Label("Select Slideshow(JSON) file: ");
        fileButton = new Button();
        fileHbox.getChildren().addAll(fileLabel, fileButton);
        fileButton.setOnAction(e-> {
            FileChooser imageFileChooser = new FileChooser();
	

        File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	   src.setText(file.toString());
	}
        
        });
        srcHbox = new HBox(5);
        srcLabel = new Label("File src: ");
        src = new Label("FILE NOT CHOSEN");
        srcHbox.getChildren().addAll(srcLabel, src);
        
        enterButton = new Button("ENTER");
        enterButton.setOnAction(e->{
        Component c = new Component();
        int width = 0;
        int height = 0;
        if (width<10) width = 10;
        if(width>1000) width = 1000;
        if(height<10) height = 10;
        if(height>1000) height =1000;
        c.setType("slideshow");
        c.setText(src.getText());
        c.setWidth(width);
        c.setHeight(height);
        c.setFont("");
        this.page.addComponent(c);
        this.close();
        });
        pane.getChildren().addAll(fileHbox, srcHbox,enterButton);
        
        primaryScene = new Scene(pane);
        this.setScene(primaryScene);
        this.showAndWait();
    }
}
