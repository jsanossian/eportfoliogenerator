/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.view;

import eportfoliogenerator.model.Component;
import eportfoliogenerator.model.EportfolioModel;
import eportfoliogenerator.model.Page;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
//
class ExportEportfolioStage  extends Stage{
    EportfolioModel eportfolio;
    Label label;
    Button ok;
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public ExportEportfolioStage(EportfolioModel eportfolio){
        this.eportfolio = eportfolio;
        try {
            this.export();
        } catch (FileNotFoundException ex) {
            
        }
    }
    private void export() throws FileNotFoundException{
        
            JsonArray pagesJsonArray = pagesJsonArray(eportfolio.getPages());
            JsonObject ep = Json.createObjectBuilder().add("pages", pagesJsonArray).build();
            String name = eportfolio.getPage(0).getStudentName();
            File html = new File("eportfoliogenerator/html/index.html");
            File htmldst = new File("./sites/" + name+ "/index.html");
            htmldst.getParentFile().mkdirs();
            try {
                Files.copy(html.toPath(), htmldst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                Logger.getLogger(ExportEportfolioStage.class.getName()).log(Level.SEVERE, null, ex);
            }
            try{
            File js = new File("./html/js/eportfolio.js");
            File jsdst = new File("./sites/" + name+ "/js/eportfolio.js");
            jsdst.getParentFile().mkdirs();
            Files.copy(js.toPath(), jsdst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            File jquery = new File("./html/js/jquery.js");
            File jquerydst = new File("./sites/" + name+ "/js/jquery.js");
            jquerydst.getParentFile().mkdirs();
            Files.copy(jquery.toPath(), jquerydst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            File ss = new File("./html/js/Slideshow.js");
            File ssdst = new File("./sites/" + name+ "/js/Slideshow.js");
            Files.copy(ss.toPath(), ssdst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            File csslayout = new File("./html/css/layout1.css");
            File csslayoutdst = new File("./sites/" + name+ "/css/layout1.css");
            csslayoutdst.getParentFile().mkdirs();
            Files.copy(csslayout.toPath(), csslayoutdst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            File csscolor = new File("./html/css/color1.css");
            File csscolordst = new File("./sites/" + name+ "/css/color1.css");
            csscolordst.getParentFile().mkdirs();
            Files.copy(csscolor.toPath(), csscolordst.toPath(), StandardCopyOption.REPLACE_EXISTING);
            
            for(Page page : eportfolio.getPages()){
                for(Component c : page.getComponents()){
                    if(c.getType().equalsIgnoreCase("image")){
                        File src = new File(c.getText());
                        File dst =new File("./sites/"+name+"/img/"+ src.getName());
                        dst.getParentFile().mkdirs();
                        try {
                            Files.copy(src.toPath(), dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException ex) {
                            
                        }
                    }
                    if(c.getType().equalsIgnoreCase("video")){
                        File src = new File(c.getText());
                        File dst =new File("./sites/"+name+"/vid/"+ src.getName());
                        dst.getParentFile().mkdirs();
                        try {
                            Files.copy(src.toPath(), dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        } catch (IOException ex) {
                            
                        }
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(ExportEportfolioStage.class.getName()).log(Level.SEVERE, null, ex);
        }
        StringWriter sw = new StringWriter();
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	 
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(ep);
	jsonWriter.close();
        String jsonFilePath = "./sites/" + name + "js/eportfolio.json";
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(ep);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
        this.label = new Label("Eportfolio has been exported to " + this.eportfolio.getPage(0).getStudentName() + "'s Website direcoty");
        this.ok = new Button("OK");
        this.setScene(new Scene(new VBox(this.label,ok)));
        this.showAndWait();
        
    }
    private JsonArray pagesJsonArray(Vector<Page> pages){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        
        for(int i = 0; i < pages.size(); i++){
            Page page = pages.get(i);
            JsonArray c = componentsJsonArray(page.getComponents()); 
            //File f = new File(page.getBanner());
            
              JsonObject jso = Json.createObjectBuilder()
                    .add("student_name", page.getStudentName())
                    .add("banner","")
                    .add("footer", page.getFooter())
                    .add("layout", page.getLayout())
                    .add("title", page.getTitle())
                    .add("color", page.getColor())
                    .add("components", c).build();
            jsb.add(jso);
        }
        
        JsonArray ja = jsb.build();
        return ja;
    }

    private JsonArray componentsJsonArray(Vector<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(int i = 0; i< components.size(); i++){
            Component component = components.get(i);
            String type = component.getType().toLowerCase();
            if (type.equals("paragraph")){
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", component.getText())
                                     .add("width", 0)
                                     .add("height", 0)
                                     .add("font", component.getFont())
                                     .build();
                jsb.add(jso);
            }
            else if (type.equals("header")){
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", component.getText())
                                     .add("width", 0)
                                     .add("height", 0)
                                     .add("font", component.getFont())
                                     .build();  
                jsb.add(jso);
            }
            else if (type.equals("list")){
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", component.getText())
                                     .add("width", 0)
                                     .add("height", 0)
                                     .add("font", component.getFont())
                                     .build(); 
                jsb.add(jso);
            }
            else if (type.equals("image")){
                File file = new File(component.getText());
                String src = "img/" + file.getName();
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", src)
                                     .add("width", component.getWidth())
                                     .add("height", component.getHeight())
                                     .add("font", "")
                                     .build();  
                jsb.add(jso);
            }
            else if (type.equals("video")){
                                File file = new File(component.getText());
                String src = "vid/" + file.getName();
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", src)
                                     .add("width", component.getWidth())
                                     .add("height", component.getHeight())
                                     .add("font", "")
                                     .build();  
                jsb.add(jso);
            }
            else if (type.equals("slideshow")){
                                File file = new File(component.getText());
                String src = "ss/" + file.getName();
                JsonObject jso = Json.createObjectBuilder()
                                     .add("type", type)
                                     .add("text", src)
                                     .add("width", 0)
                                     .add("height", 0)
                                     .add("font", "")
                                     .build();  
                jsb.add(jso);
            }
            else{
                
            }
        }
        JsonArray ja = jsb.build();
        return ja;
    }
}
