/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author jsanossian
 */
public class EportfolioModel implements Serializable{
    private Vector<Page> pages;
    Page selectedPage;

    public Page getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(Page selectedPage) {
        this.selectedPage = selectedPage;
    }
     public boolean addPage(Page p){
        return pages.add(p);
    }
    public Page getPage(int index){
        return pages.get(index);
    }
    public boolean removePage(Page c){
        return pages.remove(c);
    }
    public Page removeComponentAtIndex(int i){
        return pages.remove(i);
    }

    public EportfolioModel() {
        this.pages = new Vector<>();
    }

    public EportfolioModel(Vector<Page> pages) {
        this.pages = pages;
    }

    public Vector<Page> getPages() {
        return pages;
    }

    public void setPages(Vector<Page> pages) {
        this.pages = pages;
    }
    public boolean save(String path){
       
        try{
		   
		FileOutputStream fout = new FileOutputStream(path);
		ObjectOutputStream oos = new ObjectOutputStream(fout);   
		oos.writeObject(this);
		oos.close();
		System.out.println("Done");
                return true;
		   
	   }catch(Exception ex){
		   ex.printStackTrace();
                   return false;
	   }
    }

    
}
