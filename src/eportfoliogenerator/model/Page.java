/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import java.io.Serializable;
import java.util.Vector;

/**
 *
 * @author jsanossian
 */
public class Page implements Serializable{
    private Vector<Component> components;
    private String studentName, footer, title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    private int layout, color;

    public Page() {
        this.color = 1;
        this.layout = 1;
        this.studentName = "";
        this.footer = "";
        this.components = new Vector<>();
    }

    public Page(String studentName, String footer, int layout, int color) {
        this.studentName = studentName;
        this.footer = footer;
        this.layout = layout;
        this.color = color;
        this.components = new Vector<>();
    }
    public boolean addComponent(Component c){
        return components.add(c);
    }
    public boolean removeComponent(Component c){
        return components.remove(c);
    }
    public Component removeComponentAtIndex(int i){
        return components.remove(i);
    }
    public Vector<Component> getComponents() {
        return components;
    }

    public void setComponents(Vector<Component> components) {
        this.components = components;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
