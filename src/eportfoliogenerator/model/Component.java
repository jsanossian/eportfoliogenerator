/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliogenerator.model;

import java.io.Serializable;

/**
 *
 * @author jsanossian
 */
public class Component implements Serializable{
    private String type;
    private String text;
    private int height, width;
    private String font;

    public Component(String type, String text, String font) {
        this.type = type;
        this.text = text;
        this.font = font;
    }

    public Component(String type, String text, int height, int width, String font) {
        this.type = type;
        this.text = text;
        this.height = height;
        this.width = width;
        this.font = font;
    }

    public Component(String type, String text, int height, int width) {
        this.type = type;
        this.text = text;
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }
    public Component(){
        this.type = "";
        this.text = "";
    }
    public Component(String type){
        this.type = type;
        this.text = "";
    }
    public Component(String type, String text){
        this.type = type;
        this.text = text;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
