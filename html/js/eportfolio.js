/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var eportfolio, pages, components, selectedPage, selectedPageIndex, layoutNum, colorNum, studentName;
var eportfolioFilename = "js/eportfolio.json";
var title, navbar, banner_src, banner, contents, footer;
$.getJSON(eportfolioFilename, function (json) {
    eportfolio = json;
    initEportfolio(eportfolio);
});
var initEportfolio = function (ep) {
    pages = ep.pages;
    selectedPageIndex = 0;
    selectedPage = pages[selectedPageIndex];
    components = selectedPage.components;
    generatePage(selectedPageIndex);
    generateEventHandlers();
};
var generatePage = function (index) {
    
    page = pages[index];
    studentName = page.student_name;
    title = page.title;
    footer = page.footer;
    document.title = studentName + " | " + title;
    generateBanner(page);
    generateContent(page);
    document.getElementById("footer").innerHTML = footer;
    layoutNum = page.layout;
    colorNum = page.color;
    document.getElementById("color").href = "css/color" + colorNum + ".css";
    document.getElementById("layout").href = "css/layout" + layoutNum + ".css";
    generateNavbar();
};
var generateBanner = function (page) {
    $("#banner").empty();
    banner_src = page.banner;
    document.getElementById("banner").backgroundImage = banner_src;
    banner = studentName + " | " + title;

};
var generateContent = function (page) {
    $("#content").empty();
    contents = document.getElementById("content");
    var componentIndex = 0;
    var length = components.length;
    while (componentIndex < length) {
        generateComponent(componentIndex);
        componentIndex++;
    }
};
var generateComponent = function (index) {
   var component = components[index];
    var type = component.type.toLowerCase();
    var text = component.text;
    var height = component.height;
    var width = component.width;
    var font = component.font;
    if (type == "paragraph") {
        generateParagraph(text, font);
    }
    else if (type == "header") {
        generateHeader(text, font);
    }
    else if (type == "list") {
        generateList(text, font);
    }
    else if (type == "image") {
        generateImage(text, width, height);
    }
    else if (type == "video") {
        generateVideo(text, width, height);
    }
    else if (type == "slideshow") {
        generateSlideshow(text);
    }
    else {
        console.log("component:" + index + "  could not be generated");
    }
};

var generateParagraph = function (text, font) {
    var p = document.createElement("p");
    var ih = document.createTextNode(text);
    p.appendChild(ih);
     p.style.fontFamily = font;
    contents.appendChild(p);
   
};
var generateHeader = function (text, font) {
    var h = document.createElement("h1");
    var ih = document.createTextNode(text);
    h.appendChild(ih);
     h.style.fontFamily = font;
    contents.appendChild(h);
};
var generateList = function (text, font) {
    var list = text.split("\n");
    var length = list.length;
    var ul = document.createElement("ul");
    var index = 0;
    while (index < length) {
        var li = document.createElement("li");
        var str = document.createTextNode(list[index]);
        li.appendChild(str);
        ul.appendChild(li);
        index++;
    }
     ul.style.fontFamily = font;
    contents.appendChild(ul);
};
var generateImage = function (src, w, h) {
    var img = document.createElement("img");
    img.alt = src;
    img.src = src;
    img.width = w;
    img.height = h;
    contents.appendChild(img);
};
var generateVideo = function (text, w, h) {
    var x = document.createElement("video");
    x.setAttribute("src", text);
    x.setAttribute("width", w.toString());
    x.setAttribute("height", h.toString());
    x.setAttribute("controls", "controls");
    contents.appendChild(x);

};
var generateSlideshow = function (text) {

};
var generateNavbar = function () {
    $("#navbar").empty();
    var index = 0;
    while (index > pages.length) {
        var a = document.createElement("a");
        var str = document.createTextNode(pages[index].title);
        a.appendChild(str);
        a.href = "javaScript:void(0);";
        
        a.addEventListener("click", function () {
            selectedPageIndex = index;
            generatePage(selectedPageIndex);
        });
        if(selectedPageIndex === index){
            a.style.color = "orange";
        }
        document.getElementById("navbar").appendChild(a);
    }

};
 