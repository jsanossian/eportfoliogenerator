/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    var isPlaying = false;
    
    

    var xmlhttp = new XMLHttpRequest();
    var url = "slideshows/slideshow.json";
    var slideshow;
    var currentSlide;
    var currentSlideIndex = 0;
    var run;
    $.getJSON(url, function(json) {
        slideshow = json;
        initSlideshow(slideshow);
    });
    /*get JSON file and view it
    xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        slideshow = JSON.parse(xmlhttp.responseText);
        initSlideshow(slideshow);
        initEventHandlers();
        }
    };
    
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    */
    //initialize slideshow
    var initSlideshow = function(ss){
        var slides = ss.slides;
        currentSlide = slides[0];
    currentSlideIndex = 0;
    document.getElementById("title").innerHTML = ss.title;
    initEventHandlers();
    viewSlide(currentSlide);
    };
    
    //view slide
    var viewSlide = function(slide){
        document.getElementById("caption").innerHTML = slide.caption;
        var filename = "img/"  + slide.image_file_name;
        document.getElementById("slideimage").src = filename;
        document.getElementById("slideimage").alt = filename;
        // adjust height and width
       /* 
        * 
        *var maxH = 400
        *var w = document.getElementById("slideimage").width;
        *var aspectRatio = document.getElementById("slideimage").height / w;
        *var newW = 600 - w;
        *var newH = newW * aspectRatio;
        *if(newH > maxH){
        *    newH = maxH;
        *    newW = newH / aspectRatio;
        *}
        *document.getElementById("slideimage").width = Math.round(newW);
        *document.getElementById("slideimage").height = Math.round(newH);
        */
    };
    
    var initEventHandlers = function(){
        document.getElementById("prevbutton").addEventListener("click", prevSlide);
        document.getElementById("nextbutton").addEventListener("click", nextSlide);
        document.getElementById("playpause").addEventListener("click", playpause);
    }
    
    var prevSlide = function(){
        if(currentSlideIndex > 0){
        currentSlideIndex = currentSlideIndex - 1;
        currentSlide = slideshow.slides[currentSlideIndex];
        viewSlide(currentSlide);            
        }
        if(currentSlideIndex == 0){
            document.getElementById("prevbutton").disable = true;
        }
        else{
            document.getElementById("prevbutton").disable = false;
        }

    }
    
    var nextSlide = function(){
       if(currentSlideIndex < slideshow.slides.length - 1){
        currentSlideIndex = currentSlideIndex + 1;
        currentSlide = slideshow.slides[currentSlideIndex];
        viewSlide(currentSlide);
        }
        if(currentSlideIndex == (slideshow.slides.length -1)){
            document.getElementById("nextbutton").disable = true;
        }
        else{
            document.getElementById("nextbutton").disable = false;
        }
    }
    
    var playpause = function(){
        
        if(isPlaying == false){
            document.getElementById("playpause").textContent = "\u220E " ;
            isPlaying = true;
            playSlideshow();
           
        }
        else{
            document.getElementById("playpause").textContent ="\u25BA";
            isPlaying = false;
            clearInterval(run);
        }
    }
    
    var playSlideshow = function(){
           run = setInterval(nextSlide,3000);

        
    }

